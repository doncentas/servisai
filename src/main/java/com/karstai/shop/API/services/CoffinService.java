package com.karstai.shop.API.services;

import com.karstai.shop.API.exceptions.EntityNotFoundException;
import com.karstai.shop.API.models.Coffin;
import com.karstai.shop.API.repositories.CoffinRepository;
import com.karstai.shop.API.utils.NullAwareBeanUtilsBean;
import lombok.RequiredArgsConstructor;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CoffinService {
    private final CoffinRepository coffinRepository;

    public Coffin create(Coffin coffin) {
        return coffinRepository.save(coffin);
    }

    public Coffin getById(Long id) {
        return coffinRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Coffin not found"));
    }

    public Coffin update(Long id, Coffin coffin) {
        Coffin dbCoffin = coffinRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Coffin not found"));
        coffin.setId(dbCoffin.getId());

        try {
            BeanUtils.copyProperties(dbCoffin, coffin);
        } catch (Exception e) {
            throw new RuntimeException("ERROR");
        }

        return coffinRepository.save(dbCoffin);
    }

    public Coffin patchUpdate(Long id, Coffin coffin) {
        BeanUtilsBean beanUtilsBean = new NullAwareBeanUtilsBean();
        Coffin dbCoffin = coffinRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Coffin not found"));

        try {
            beanUtilsBean.copyProperties(dbCoffin, coffin);
        } catch (Exception e) {
            throw new RuntimeException("ERROR");
        }

        return coffinRepository.save(dbCoffin);
    }

    public List<Coffin> getAll() {
        return coffinRepository.findAll();
    }

    public void delete(Long id) {
        if(!coffinRepository.existsById(id)) {
            throw new EntityNotFoundException("Coffin not found");
        }
        coffinRepository.deleteById(id);
    }
}
